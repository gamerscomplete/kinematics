package kinematics

import (
	"fmt"
	"math"
)

/*
func main() {
	// Example lengths (same as from the IK example)
	coxaLen := 30.0
	femurLen := 50.0
	tibiaLen := 80.0

	// Example target position
	xTarget := 100.0
	yTarget := 20.0
	zTarget := -110.0

	coxaAngle, femurAngle, tibiaAngle, err := InverseKinematics(xTarget, yTarget, zTarget, coxaLen, femurLen, tibiaLen)
	if err != nil {
		fmt.Println("IK Error:", err)
		return
	}

	// Now use the FK to check if we get back (xTarget, yTarget, zTarget)
	Fx, Fy, Fz := ForwardKinematics(coxaAngle, femurAngle, tibiaAngle, coxaLen, femurLen, tibiaLen)

	fmt.Printf("Original Target: (%f, %f, %f)\n", xTarget, yTarget, zTarget)
	fmt.Printf("FK Result:       (%f, %f, %f)\n", Fx, Fy, Fz)
	fmt.Printf("Coxa Angle:  %f deg\n", coxaAngle*180/math.Pi)
	fmt.Printf("Femur Angle: %f deg\n", femurAngle*180/math.Pi)
	fmt.Printf("Tibia Angle: %f deg\n", tibiaAngle*180/math.Pi)

    maxRadius, maxUp, maxDown := MaxReachableBounds(coxaLen, femurLen, tibiaLen)
    fmt.Printf("Max horizontal radius: %f\n", maxRadius)
    fmt.Printf("Max vertical up: %f\n", maxUp)
    fmt.Printf("Max vertical down: %f\n", maxDown)
}
*/
// ForwardKinematics calculates the foot position given the joint angles and segment lengths.
// Angles are in radians and lengths in consistent units (e.g., mm).
func ForwardKinematics(coxaAngle, femurAngle, tibiaAngle float64,
	coxaLength, femurLength, tibiaLength float64) (float64, float64, float64) {

	// Convert tibiaAngle to an absolute angle from horizontal
	// based on how it was defined in the IK code:
	//	tibiaAbsAngle := femurAngle + (tibiaAngle - math.Pi)
	tibiaAbsAngle := femurAngle + tibiaAngle

	// Coxa end position
	CEx := coxaLength * math.Cos(coxaAngle)
	CEy := coxaLength * math.Sin(coxaAngle)
	CEz := 0.0

	// Femur end position
	FGx := CEx + femurLength*math.Cos(femurAngle)*math.Cos(coxaAngle)
	FGy := CEy + femurLength*math.Cos(femurAngle)*math.Sin(coxaAngle)
	FGz := CEz - femurLength*math.Sin(femurAngle)

	// Foot position
	Fx := FGx + tibiaLength*math.Cos(tibiaAbsAngle)*math.Cos(coxaAngle)
	Fy := FGy + tibiaLength*math.Cos(tibiaAbsAngle)*math.Sin(coxaAngle)
	Fz := FGz - tibiaLength*math.Sin(tibiaAbsAngle)

	return Fx, Fy, Fz
}

// InverseKinematics calculates the joint angles for a single hexapod leg.
// Given a desired foot position (x, y, z) relative to the coxa joint origin
// and leg segment lengths (coxaLength, femurLength, tibiaLength),
// it returns the coxaAngle, femurAngle, and tibiaAngle in radians.
//
// Coordinate system assumption:
// - x: forward direction
// - y: lateral direction (positive to the left or right depending on leg side)
// - z: vertical direction (down could be negative depending on your setup)
//
// Angles are given such that:
// - coxaAngle is the rotation about the vertical axis (yaw)
// - femurAngle is rotation in a vertical plane after coxa
// - tibiaAngle is rotation in a vertical plane after femur
func InverseKinematics(x, y, z float64, coxaLength, femurLength, tibiaLength float64) (float64, float64, float64, error) {
	// Small epsilon for checking invalid configurations
	const epsilon = 1e-9

	// 1. Compute the coxa angle
	coxaAngle := math.Atan2(y, x)

	// 2. Distance from coxa joint ignoring its length
	// First, find horizontal planar distance to foot
	planarDist := math.Sqrt(x*x + y*y)
	if planarDist < coxaLength+epsilon {
		return 0, 0, 0, fmt.Errorf("target too close to coxa base, no valid solution")
	}

	// Adjusted horizontal distance after coxa
	horizontalDist := planarDist - coxaLength

	// 3. The distance from the femur joint to the foot
	legDist := math.Sqrt(horizontalDist*horizontalDist + z*z)

	// Check for reachability
	if legDist > femurLength+tibiaLength || legDist < math.Abs(femurLength-tibiaLength) {
		return 0, 0, 0, fmt.Errorf("target out of reachable range")
	}

	// 4. Law of Cosines for femur angle (F) and tibia angle (T)
	// cosF: angle at femur joint
	cosF := (femurLength*femurLength + legDist*legDist - tibiaLength*tibiaLength) / (2.0 * femurLength * legDist)
	if cosF > 1 || cosF < -1 {
		return 0, 0, 0, fmt.Errorf("invalid cosF: %f, no valid solution", cosF)
	}

	// cosT: angle at tibia joint
	cosT := (femurLength*femurLength + tibiaLength*tibiaLength - legDist*legDist) / (2.0 * femurLength * tibiaLength)
	if cosT > 1 || cosT < -1 {
		return 0, 0, 0, fmt.Errorf("invalid cosT: %f, no valid solution", cosT)
	}

	// Femur angle is composed of two parts:
	// - The angle from horizontalDist/z line (atan2)
	//   plus the angle given by acos(cosF)

	baseAngle := math.Atan2(-z, horizontalDist)
	femurAngle := baseAngle - math.Acos(cosF)
	//	femurAngle := -math.Atan2(z, horizontalDist) + math.Acos(cosF)

	// Tibia angle is simply π - acos(cosT)
	tibiaAngle := math.Pi - math.Acos(cosT)

	return coxaAngle, femurAngle, tibiaAngle, nil
}

func MaxReachableBounds(coxaLength, femurLength, tibiaLength float64) (maxRadius, maxUp, maxDown float64) {
	maxRadius = coxaLength + femurLength + tibiaLength
	maxUp = femurLength + tibiaLength
	maxDown = -(femurLength + tibiaLength)
	return
}
